﻿namespace AmpacheApp.Common
{
    public class Tag
    {
        public string id { get; set; }
        public string name { get; set; }
    }
}
