﻿using AmpacheApiWrapper;
using AmpacheApiWrapper.DataTypes;
using AmpacheApp.Models;
using AmpacheApp.Utilities;
using AmpacheApp.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace AmpacheApp.ViewModels
{
    public interface IAlbumViewModel
    {
        ObservableCollection<Song> Songs { get; }
        Command LoadSongsCommand { get; }
        Command<Song> SongTapped { get; }
        Song SelectedSong { get; set; }

        void OnAppearing();
        void OnReAppearing();
    }

    public class AlbumViewModel : BaseViewModel, IAlbumViewModel
    {
        private Song _selectedSong;
        private Album _currentAlbum;
        private readonly IAmpacheApi _ampacheApi;
        private readonly IPlaylist _playlist;
        private readonly IAmpacheServerData _ampacheServerData;
        private bool _isInitalised = false;

        public Album CurrentAlbum
        {
            get
            {
                return _currentAlbum;
            }
            set
            {
                SetProperty(ref _currentAlbum, value);
            }
        }
        public Song SelectedSong
        {
            get => _selectedSong;
            set
            {
                SetProperty(ref _selectedSong, value);
                OnSongSelected(value);
            }
        }

        public ObservableCollection<Song> Songs { get; }
        public Command LoadSongsCommand { get; }
        public Command<Song> SongTapped { get; }
        

        public AlbumViewModel(IAmpacheApi ampacheApi, IPlaylist playlist, IAmpacheServerData ampacheServerData)
        {
            _ampacheApi = ampacheApi;
            _playlist = playlist;
            _ampacheServerData = ampacheServerData;

            Title = "Browse Albums";
            Songs = new ObservableCollection<Song>();

            LoadSongsCommand = new Command(async () => await ExecuteLoadAlbumsCommand());
            SongTapped = new Command<Song>(OnSongSelected);
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedSong = null;
        }

        public void OnReAppearing()
        {
            SelectedSong = null;
        }

        private async Task ExecuteLoadAlbumsCommand()
        {
            IsBusy = true;

            try
            {
                Songs.Clear();

                if(!_isInitalised)
                {
                    if (_ampacheServerData.TryGetCurrAlbum(out var currAlbum, out var songs))
                    {
                        CurrentAlbum = currAlbum;
                        songs.ForEach(Songs.Add);
                    }
                    else
                    {
                        await DownloadAlbum();
                    }

                    _isInitalised = true;
                }
                else
                {
                    await DownloadAlbum();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                _playlist.SetPlaylistSongs(Songs);
                IsBusy = false;
            }
        }

        private async Task DownloadAlbum()
        {
            List<Song> songList = null;

            await Task.Run(() =>
            {
                songList = _ampacheApi.GetAlbumSongs(_ampacheServerData.CurrentAlbum);
                _ampacheServerData.InsertSongs(songList);
                Preferences.Set(Constants.SongListPreference, JsonConvert.SerializeObject(_ampacheServerData.GetSongs()));

                if (_ampacheServerData.TryGetCurrAlbum(out var album))
                {
                    CurrentAlbum = album;
                }
                else
                {
                    CurrentAlbum = _ampacheApi.GetAlbum(_ampacheServerData.CurrentAlbum);
                    _ampacheServerData.InsertAlbum(CurrentAlbum);
                    Preferences.Set(Constants.SongListPreference, JsonConvert.SerializeObject(_ampacheServerData.GetAlbums()));
                }
            });

            songList.ForEach(Songs.Add);
        }

        private async void OnSongSelected(Song song)
        {
            if (song == null)
                return;

            _playlist.SetCurrent(song.id);

            await Shell.Current.GoToAsync($"{nameof(MusicPlayerPage)}");
            return;
        }
    }
}
