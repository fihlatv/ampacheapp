﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;
using AmpacheApp.Views;
using AmpacheApiWrapper.DataTypes;
using AmpacheApiWrapper;
using AmpacheApp.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using Xamarin.Essentials;
using AmpacheApp.Utilities;

namespace AmpacheApp.ViewModels
{
    public interface IArtistViewModel
    {
        Artist CurrentArtist { get; set; }
        Album SelectedAlbum { get; set; }
        ObservableCollection<Album> Albums { get; }
        Command LoadAlbumsCommand { get; }
        Command<Album> AlbumTapped { get; }

        void OnAppearing();
        void OnReAppearing();
    }

    public class ArtistViewModel : BaseViewModel, IArtistViewModel
    {
        private Album _tappedAlbum;
        private Artist _currentArtist;
        private readonly IAmpacheApi _ampacheApi;
        private readonly IAmpacheServerData _ampacheServerData;
        private bool _isInitalised = false;

        public Artist CurrentArtist
        {
            get => _currentArtist;
            set => SetProperty(ref _currentArtist, value);
        }
        public Album SelectedAlbum
        {
            get => _tappedAlbum;
            set
            {
                SetProperty(ref _tappedAlbum, value);
                OnAlbumSelected(value);
            }
        }
        public ObservableCollection<Album> Albums { get; }
        public Command LoadAlbumsCommand { get; }
        public Command<Album> AlbumTapped { get; }

        public ArtistViewModel(IAmpacheApi ampacheApi, IAmpacheServerData ampacheServerData)
        {
            _ampacheApi = ampacheApi;
            _ampacheServerData = ampacheServerData;
            Title = "Browse Albums";
            Albums = new ObservableCollection<Album>();
            LoadAlbumsCommand = new Command(async () => await ExecuteLoadAlbumsCommand());
            AlbumTapped = new Command<Album>(OnAlbumSelected);
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedAlbum = null;
        }

        public void OnReAppearing()
        {
            SelectedAlbum = null;
        }

        private async Task ExecuteLoadAlbumsCommand()
        {
            IsBusy = true;

            try
            {
                Albums.Clear();

                if (!_isInitalised)
                {
                    if (_ampacheServerData.TryGetCurrArtist(out var currArtist, out var currAlbums))
                    {
                        CurrentArtist = currArtist;
                        currAlbums.ForEach(Albums.Add);
                    }
                    else
                    {
                        await DownloadArtist();
                    }

                    _isInitalised = true;
                }
                else
                {
                    await DownloadArtist();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task DownloadArtist()
        {
            List<Album> albumList = null;

            await Task.Run(() =>
            {
                albumList = _ampacheApi.GetArtistAlbums(_ampacheServerData.CurrentArtist);

                if(_ampacheServerData.TryGetCurrArtist(out var artist))
                {
                    CurrentArtist = artist;
                }
                else
                {
                    CurrentArtist = _ampacheApi.GetArtist(_ampacheServerData.CurrentArtist);
                    _ampacheServerData.InsertArtist(CurrentArtist);
                    Preferences.Set(Constants.ArtistListPreference, JsonConvert.SerializeObject(_ampacheServerData.GetArtists()));
                }

                _ampacheServerData.InsertAlbums(albumList);
                Preferences.Set(Constants.AlbumListPreference, JsonConvert.SerializeObject(_ampacheServerData.GetAlbums()));
            });

            albumList.ForEach(Albums.Add);
        }

        private async void OnAlbumSelected(Album album)
        {
            if (album == null)
                return;

            _ampacheServerData.CurrentAlbum = album.id;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(AlbumPage)}");
            return;
        }
    }
}