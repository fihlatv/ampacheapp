﻿using AmpacheApiWrapper;
using AmpacheApiWrapper.DataTypes;
using AmpacheApp.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AmpacheApp.ViewModels
{
    public interface IMusicPlayerViewModel
    {
        bool IsPaused { get; set; }
        bool IsPlaying { get; set; }
        Song CurrentSong { get; set; }
        Album CurrentAlbum { get; set; }
        Command PlaySongCommand { get; }
        Command PauseSongCommand { get; }
        Command StopSongsCommand { get; }
        Command PrevSongCommand { get; }
        Command NextSongCommand { get; }
        void OnAppearing();
    }

    public class MusicPlayerViewModel : BaseViewModel, IMusicPlayerViewModel
    {
        private bool _isPlaying;
        private bool _isPaused;
        private Song _currentSong;
        private Album _currentAlbum;
        private readonly IMusicPlayer _musicPlayer;
        private readonly IAmpacheApi _ampacheApi;
        private readonly IPlaylist _playlist;

        public bool IsPaused
        {
            get => _isPaused;
            set => SetProperty(ref _isPaused, value);
        }

        public bool IsPlaying
        {
            get => _isPlaying;
            set
            {
                SetProperty(ref _isPlaying, value);
                IsPaused = !_isPlaying;
            }
        }

        public Song CurrentSong
        {
            get => _currentSong;
            set => SetProperty(ref _currentSong, value);
        }

        public Album CurrentAlbum 
        {
            get => _currentAlbum;
            set => SetProperty(ref _currentAlbum, value);
        }
        public Command PlaySongCommand { get; }
        public Command PauseSongCommand { get; }
        public Command StopSongsCommand { get; }
        public Command PrevSongCommand { get; }
        public Command NextSongCommand { get; }

        public MusicPlayerViewModel(IMusicPlayer musicPlayer, IAmpacheApi ampacheApi, IPlaylist playlist)
        {
            _musicPlayer = musicPlayer;
            _ampacheApi = ampacheApi;
            _playlist = playlist;

            Title = "Browse Albums";
            LoadAlbumCommand().GetAwaiter();
            PlaySongCommand = new Command(() =>
            {
                _musicPlayer.Play();
                IsPlaying = true;
            });
            PauseSongCommand = new Command(() => 
            {
                _musicPlayer.Pause();
                IsPlaying = false;
            });
            StopSongsCommand = new Command(() =>
            {
                _musicPlayer.Stop();
                IsPlaying = false;
            });
            PrevSongCommand = new Command(() =>
            {
                CurrentSong = _playlist.Previous();
                _musicPlayer.StartSong(CurrentSong.url);
            });
            NextSongCommand = new Command(() =>
            {
                CurrentSong = _playlist.Next();
                _musicPlayer.StartSong(CurrentSong.url);
            });
        }

        public void OnAppearing()
        {
            IsBusy = true;
        }

        private async Task LoadAlbumCommand()
        {
            IsBusy = true;

            try
            {
                await Task.Run(() =>
                {
                    CurrentSong = _playlist.CurrentSong();
                    CurrentAlbum = _ampacheApi.GetAlbum(CurrentSong.album.id);
                    _musicPlayer.StartSong(CurrentSong.url);
                    IsPlaying = true;
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
