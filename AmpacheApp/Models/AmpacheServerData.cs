﻿using AmpacheApiWrapper.DataTypes;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms.Internals;

namespace AmpacheApp.Models
{
    public interface IAmpacheServerData
    {
        long CurrentArtist { get; set; }
        long CurrentAlbum { get; set; }

        void InsertArtist(Artist artist);
        void InsertArtists(IEnumerable<Artist> artists);
        void InsertAlbum(Album album);
        void InsertAlbums(IEnumerable<Album> albums);
        void InsertSong(Song song);
        void InsertSongs(IEnumerable<Song> songs);

        bool TryGetArtistAlbums(long artistId, out List<Album> albums);
        bool TryGetAlbumSongs(long albumId, out List<Song> songs);

        bool TryGetCurrArtist(out Artist artist);
        bool TryGetCurrArtist(out Artist artist, out List<Album> albums);

        bool TryGetArtist(long artistId, out Artist artist);
        bool TryGetArtist(long artistId, out Artist artist, out List<Album> albums);

        bool TryGetCurrAlbum(out Album album);
        bool TryGetCurrAlbum(out Album album, out List<Song> songs);

        bool TryGetAlbum(long albumId, out Album album);
        bool TryGetAlbum(long albumId, out Album album, out List<Song> songs);

        List<Artist> GetArtists();
        List<Album> GetAlbums();
        List<Song> GetSongs();
    }

    public class AmpacheServerData : IAmpacheServerData
    {
        private readonly Dictionary<long, Artist> _artists;
        private readonly Dictionary<long, Album> _albums;
        private readonly Dictionary<long, Song> _songs;


        public long CurrentArtist { get; set; }
        public long CurrentAlbum { get; set; }

        public AmpacheServerData()
        {
            _artists = new Dictionary<long, Artist>();
            _albums = new Dictionary<long, Album>();
            _songs = new Dictionary<long, Song>();
        }

        public void InsertArtist(Artist artist)
        {
            _artists[artist.id] = artist;
        }

        public void InsertArtists(IEnumerable<Artist> artists)
        {
            foreach(Artist artist in artists)
            {
                InsertArtist(artist);
            }
        }

        public void InsertAlbum(Album album)
        {
            _albums[album.id] = album;
        }

        public void InsertAlbums(IEnumerable<Album> albums)
        {
            foreach(Album album in albums)
            {
                InsertAlbum(album);
            }
        }

        public void InsertSong(Song song)
        {
            _songs[song.id] = song;
        }

        public void InsertSongs(IEnumerable<Song> songs)
        {
            foreach (Song song in songs)
            {
                InsertSong(song);
            }
        }

        public bool TryGetArtistAlbums(long artistId, out List<Album> artistsAlbums)
        {
            artistsAlbums = new List<Album>();

            foreach (var album in _albums.Values)
            {
                if(album.artist.id == artistId)
                {
                    artistsAlbums.Add(album);
                }

                artistsAlbums = artistsAlbums.OrderBy(x => x.year).ToList();
            }

            return artistsAlbums.Any();
        }

        public bool TryGetAlbumSongs(long albumId, out List<Song> albumSongs)
        {
            albumSongs = new List<Song>();

            foreach (var song in _songs.Values)
            {
                if (song.album.id == albumId)
                {
                    albumSongs.Add(song);
                }

                albumSongs = albumSongs.OrderBy(x => x.track).ToList();
            }

            return albumSongs.Any();
        }

        public bool TryGetCurrArtist(out Artist artist)
        {
            return TryGetArtist(CurrentArtist, out artist);
        }

        public bool TryGetCurrArtist(out Artist artist, out List<Album> albums)
        {
            return TryGetArtist(CurrentArtist, out artist, out albums);
        }

        public bool TryGetArtist(long artistId, out Artist artist)
        {
            return _artists.TryGetValue(artistId, out artist);
        }

        public bool TryGetArtist(long artistId, out Artist artist, out List<Album> albums)
        {
            albums = null;

            return TryGetArtist(artistId, out artist) && TryGetArtistAlbums(artistId, out albums);
        }

        public bool TryGetCurrAlbum(out Album album)
        {
            return _albums.TryGetValue(CurrentAlbum, out album);
        }

        public bool TryGetCurrAlbum(out Album album, out List<Song> songs)
        {
            return TryGetAlbum(CurrentAlbum, out album, out songs);
        }

        public bool TryGetAlbum(long albumId, out Album album)
        {
            return _albums.TryGetValue(albumId, out album);
        }

        public bool TryGetAlbum(long albumId, out Album album, out List<Song> songs)
        {
            songs = null;

            return TryGetAlbum(albumId, out album) && TryGetAlbumSongs(albumId, out songs);
        }

        public List<Artist> GetArtists()
        {
            return _artists.Values.OrderBy(x => x.name).ToList();
        }

        public List<Album> GetAlbums()
        {
            return _albums.Values.OrderBy(x => x.name).ToList();
        }

        public List<Song> GetSongs()
        {
            return _songs.Values.OrderBy(x => x.name).ToList();
        }
    }
}
