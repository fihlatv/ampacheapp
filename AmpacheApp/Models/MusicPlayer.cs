﻿using Android.Media;

namespace AmpacheApp.Models
{
    public interface IMusicPlayer
    {
        void StartSong(string filePath);
        void Play();
        void Pause();
        void Stop();
    }

    public class MusicPlayer: IMusicPlayer
    {
        protected MediaPlayer _mediaPlayer;

        public MusicPlayer()
        {
            _mediaPlayer = new MediaPlayer();
        }


        public void StartSong(string filePath)
        {
            _mediaPlayer.Reset();
            _mediaPlayer.SetDataSource(filePath);
            _mediaPlayer.Prepare();
            _mediaPlayer.Start();
        }

        public void Play()
        {
            _mediaPlayer.Start();
        }

        public void Pause()
        {
            _mediaPlayer.Pause();
        }

        public void Stop()
        {
            _mediaPlayer.Stop();
        }
    }
}
