﻿using AmpacheApiWrapper.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AmpacheApp.Models
{
    public interface IPlaylist
    {
        void SetPlaylistSongs(IEnumerable<Song> songs);
        Song CurrentSong();
        Song Next();
        Song Previous();
        Song SetCurrent(long songId);
    }

    public class Playlist : IPlaylist
    {
        private List<Song> _songs;
        private int _currSongIndex;

        public Playlist()
        {
            _songs = new List<Song>();
            _currSongIndex = 0;
        }

        public Playlist(IEnumerable<Song> songs)
        {
            _songs = songs.ToList();
            _currSongIndex = 0;
        }

        public void SetPlaylistSongs(IEnumerable<Song> songs)
        {
            _songs = songs.ToList();
        }

        public Song CurrentSong()
        {
            return _songs[_currSongIndex];
        }

        public Song Next()
        {
            _currSongIndex = (_currSongIndex + 1) % _songs.Count;
            return CurrentSong();
        }

        public Song Previous()
        {
            _currSongIndex--;
            if (_currSongIndex < 0)
            {
                _currSongIndex = _songs.Count + _currSongIndex;
            }

            return CurrentSong();
        }

        public Song SetCurrent(long songId)
        {
            int newIndex = 0;

            foreach(Song song in _songs)
            {
                if(song.id == songId)
                {
                    _currSongIndex = newIndex;
                    return CurrentSong();
                }
                newIndex++;
            }

            throw new ArgumentOutOfRangeException("The song ID provided does not exist in this playlist");
        }
    }
}
