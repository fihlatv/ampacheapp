﻿namespace AmpacheApp.Utilities
{
    public static class Constants
    {
        public static readonly string AmpacheServerDefaultValue = nameof(AmpacheServerDefaultValue);
        public static readonly string ArtistListPreference = nameof(ArtistListPreference);
        public static readonly string AlbumListPreference = nameof(AlbumListPreference);
        public static readonly string SongListPreference = nameof(SongListPreference);
    }
}
