﻿using System;
using AmpacheApp.Views;
using Xamarin.Forms;

namespace AmpacheApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
            Routing.RegisterRoute(nameof(ArtistPage), typeof(ArtistPage));
            Routing.RegisterRoute(nameof(AlbumPage), typeof(AlbumPage));
            Routing.RegisterRoute(nameof(MusicPlayerPage), typeof(MusicPlayerPage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//LoginPage");
        }
    }
}
