﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AmpacheApp.ViewModels;
using Nancy.TinyIoc;

namespace AmpacheApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArtistListPage : ContentPage
    {
        readonly IArtistListViewModel _viewModel;
        bool _isInitialised = false;

        public ArtistListPage()
        {
            if (!_isInitialised)
            {
                InitializeComponent();
                BindingContext = _viewModel = TinyIoCContainer.Current.Resolve<IArtistListViewModel>();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!_isInitialised)
            {
                _viewModel.OnAppearing();
                _isInitialised = true;
            }
            else
            {
                _viewModel.OnReAppearing();
            }
        }
    }
}
