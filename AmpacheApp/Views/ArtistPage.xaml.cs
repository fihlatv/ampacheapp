﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using AmpacheApp.ViewModels;
using Nancy.TinyIoc;

namespace AmpacheApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArtistPage : ContentPage
    {
        readonly IArtistViewModel _viewModel;
        bool _isInitialised = false;

        public ArtistPage()
        {
            if (!_isInitialised)
            {
                InitializeComponent();
                BindingContext = _viewModel = TinyIoCContainer.Current.Resolve<IArtistViewModel>();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!_isInitialised)
            {
                _viewModel.OnAppearing();
                _isInitialised = true;
            }
            else
            {
                _viewModel.OnReAppearing();
            }
        }
    }
}

