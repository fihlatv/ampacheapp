﻿using AmpacheApp.ViewModels;
using Nancy.TinyIoc;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmpacheApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AlbumPage : ContentPage
    {
        IAlbumViewModel _viewModel;
        bool _isInitialised = false;

        public AlbumPage()
        {
            if (!_isInitialised)
            {
                InitializeComponent();
                BindingContext = _viewModel = TinyIoCContainer.Current.Resolve<IAlbumViewModel>();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!_isInitialised)
            {
                _viewModel.OnAppearing();
                _isInitialised = true;
            }
            else
            {
                _viewModel.OnReAppearing();
            }
        }
    }
}