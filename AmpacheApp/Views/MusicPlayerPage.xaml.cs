﻿using AmpacheApp.ViewModels;
using Nancy.TinyIoc;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AmpacheApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MusicPlayerPage : ContentPage
    {
        IMusicPlayerViewModel _viewModel;
        bool _isInitialised = false;
        public double screenWidth = DeviceDisplay.MainDisplayInfo.Width / DeviceDisplay.MainDisplayInfo.Density;

        public MusicPlayerPage()
        {
            if (!_isInitialised)
            {
                InitializeComponent();
                BindingContext = _viewModel = TinyIoCContainer.Current.Resolve<IMusicPlayerViewModel>();
                topGridRow.BindingContext = screenWidth;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (!_isInitialised)
            {
                _viewModel.OnAppearing();
                _isInitialised = true;
            }
        }


    }
}