﻿using AmpacheApiWrapper.DataTypes;
using AmpacheApp.Models;
using Bogus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AmpacheServerDataTests
    {
        public TestContext TestContext { get; set; }
        private Faker _faker = new Faker();

        [TestMethod]
        public void CreateSutTest()
        {
            //Arrange
            IAmpacheServerData sut;

            //Act
            sut = CreateSut();

            //Assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void InsertArtistTest()
        {
            //Arrange
            IAmpacheServerData sut = CreateSut();

            //Act
            sut.InsertArtists(AmpacheServerConst.ArtistList);

            //Assert
            Assert.IsTrue(sut.TryGetArtist(AmpacheServerConst.Artist1Id, out var artist));
            Assert.IsNotNull(artist);
            Assert.IsTrue(sut.TryGetArtist(AmpacheServerConst.Artist2Id, out artist));
            Assert.IsNotNull(artist);
            Assert.IsTrue(sut.TryGetArtist(AmpacheServerConst.Artist3Id, out artist));
            Assert.IsNotNull(artist);
        }

        [TestMethod]
        public void InsertAlbumsTest()
        {
            //Arrange
            IAmpacheServerData sut = CreateSut();

            //Act
            sut.InsertAlbums(AmpacheServerConst.AlbumList);

            //Assert
            Assert.IsTrue(sut.TryGetArtistAlbums(AmpacheServerConst.Artist1Id, out var albums));
            Assert.AreEqual(1, albums.Count);
            Assert.IsTrue(sut.TryGetArtistAlbums(AmpacheServerConst.Artist2Id, out albums));
            Assert.AreEqual(2, albums.Count);
            Assert.IsTrue(sut.TryGetArtistAlbums(AmpacheServerConst.Artist3Id, out albums));
            Assert.AreEqual(1, albums.Count);
        }

        [TestMethod]
        public void InsertSongsTest()
        {
            //Arrange
            IAmpacheServerData sut = CreateSut();

            //Act
            sut.InsertSongs(AmpacheServerConst.SongList);

            //Assert
            Assert.IsTrue(sut.TryGetAlbumSongs(AmpacheServerConst.Album1Id, out var songs));
            Assert.AreEqual(2, songs.Count);
            Assert.IsTrue(sut.TryGetAlbumSongs(AmpacheServerConst.Album2Id, out songs));
            Assert.AreEqual(2, songs.Count);
            Assert.IsTrue(sut.TryGetAlbumSongs(AmpacheServerConst.Album3Id, out songs));
            Assert.AreEqual(2, songs.Count);
            Assert.IsTrue(sut.TryGetAlbumSongs(AmpacheServerConst.Album4Id, out songs));
            Assert.AreEqual(3, songs.Count);
        }

        [TestMethod]
        public void TryGetArtistWithAlbumsTest()
        {
            //Arrange
            IAmpacheServerData sut = CreateSut();

            //Act
            sut.InsertArtists(AmpacheServerConst.ArtistList);
            sut.InsertAlbums(AmpacheServerConst.AlbumList);

            //Assert
            Assert.IsTrue(sut.TryGetArtist(AmpacheServerConst.Artist2Id, out var artist, out var albums));
            Assert.IsNotNull(artist);
            Assert.AreEqual(2, albums.Count);
        }

        [TestMethod]
        public void TryGetAlbumWithSongsTest()
        {
            //Arrange
            IAmpacheServerData sut = CreateSut();

            //Act
            sut.InsertArtists(AmpacheServerConst.ArtistList);
            sut.InsertAlbums(AmpacheServerConst.AlbumList);
            sut.InsertSongs(AmpacheServerConst.SongList);

            //Assert
            Assert.IsTrue(sut.TryGetAlbum(AmpacheServerConst.Album1Id, out var album, out var songs));
            Assert.IsNotNull(album);
            Assert.AreEqual(2, songs.Count);
        }

        private IAmpacheServerData CreateSut()
        {
            return new AmpacheServerData();
        }
    }
}
