﻿using AmpacheApiWrapper.DataTypes;
using AmpacheApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;

namespace UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class PlaylistTests
    {
        [TestMethod]
        public void CreateSutTest()
        {
            //Arrange
            IPlaylist sut;

            //Act
            sut = new Playlist();

            //Assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void CreateSutTest_WithSongList()
        {
            //Arrange
            IPlaylist sut;

            //Act
            sut = new Playlist(AmpacheServerConst.SongList);

            //Assert
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void CurrSongTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            Song song = sut.CurrentSong();

            //Assert
            Assert.IsNotNull(song);
            Assert.AreEqual(AmpacheServerConst.Song1Id, song.id);
        }

        [TestMethod]
        public void NextTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            Song actual = sut.Next();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song2Id, actual.id);
        }

        [TestMethod]
        public void PreviousTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            sut.Next();
            Song actual = sut.Previous();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song1Id, actual.id);
        }

        [TestMethod]
        public void NextLoopTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            sut.Next();
            sut.Next();
            sut.Next();
            sut.Next();
            sut.Next();
            sut.Next();
            sut.Next();
            sut.Next();
            Song actual = sut.Next();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song1Id, actual.id);
        }

        [TestMethod]
        public void PreviousLoopTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            Song actual = sut.Previous();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song9Id, actual.id);
        }

        [TestMethod]
        public void RepeatedLoopTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            sut.Previous();
            Song actual = sut.Next();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song1Id, actual.id);
        }

        [TestMethod]
        public void RepeatedLoopTestX4()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            sut.Previous();
            sut.Next();
            sut.Previous();
            sut.Next();
            sut.Previous();
            sut.Next();
            sut.Previous();
            Song actual = sut.Next();

            //Assert
            Assert.IsNotNull(actual);
            Assert.AreEqual(AmpacheServerConst.Song1Id, actual.id);
        }

        [TestMethod]
        public void SetCurrentSongTest()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            Assert.AreEqual(AmpacheServerConst.Song1Id, sut.SetCurrent(AmpacheServerConst.Song1Id).id);
            Assert.AreEqual(AmpacheServerConst.Song2Id, sut.SetCurrent(AmpacheServerConst.Song2Id).id);
            Assert.AreEqual(AmpacheServerConst.Song3Id, sut.SetCurrent(AmpacheServerConst.Song3Id).id);
            Assert.AreEqual(AmpacheServerConst.Song4Id, sut.SetCurrent(AmpacheServerConst.Song4Id).id);

            //Assert
            //We mostly we dont want to throw an exception
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SetCurrentSongTest_BadId()
        {
            //Arrange
            IPlaylist sut = new Playlist(AmpacheServerConst.SongList);

            //Act
            sut.SetCurrent(100);

            //Assert
            //We mostly we dont want to throw an exception
        }
    }
}
