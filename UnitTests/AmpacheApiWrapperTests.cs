using AmpacheApiWrapper;
using AmpacheApiWrapper.DataTypes;
using AmpacheApiWrapper.DataTypes.Authentication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace UnitTests
{
    [ExcludeFromCodeCoverage]
    [TestClass]
    public class AmpacheApiWrapperTests
    {
        public TestContext TestContext { get; set; }
        private IAmpacheAuthInfo _ampacheAuthInfo = new AmpacheAuthInfo
        {
            AmpacheUrl = "https://drdoomsalot-music.com",
            UserName = "test",
            Password = "test"
        };

        [TestCleanup]
        public void LogoutOfAmpache()
        {
            //I might implement this eventually
        }


        [TestMethod]
        public void CreateSutTest()
        {
            IAmpacheApi sut = CreateSut();
            Assert.IsNotNull(sut);
        }

        [TestMethod]
        public void LoginTest_GoodLogin()
        {
            //Arrange
            IAmpacheApi sut = CreateSut();

            //Act
            bool actual = sut.Login();

            //Assert
            Assert.IsTrue(actual);
            Assert.IsNotNull(_ampacheAuthInfo.AuthToken);
        }

        [TestMethod]
        public void LoginTest_BadLogin()
        {
            //Arrange
            IAmpacheAuthInfo badLogin = new AmpacheAuthInfo();
            IAmpacheApi sut = CreateSut(badLogin);

            //Act
            bool actual = sut.Login();

            //Assert
            Assert.IsFalse(actual);
            Assert.IsNull(badLogin.AuthToken);
        }

        [TestMethod]
        public void GetAllArtistsTest()
        {
            //Arrange
            IAmpacheApi sut = CreateSut(_ampacheAuthInfo);

            //Act
            List<Artist> artists = sut.GetAllArtists();

            //Assert
            Assert.IsNotNull(artists);
            Assert.IsTrue(artists.Count > 0);
        }

        [TestMethod]
        public void GetAllArtistAlbumsTest()
        {
            //Arrange
            IAmpacheApi sut = CreateSut();

            //Act
            List<Artist> artists = sut.GetAllArtists();
            List<Album> albums = sut.GetArtistAlbums(artists.First().id);

            //Assert
            Assert.IsNotNull(albums);
            Assert.IsTrue(albums.Count > 0);
        }

        [TestMethod]
        public void GetAllAlbumSongsTest()
        {
            //Arrange
            IAmpacheApi sut = CreateSut();

            //Act
            List<Artist> artists = sut.GetAllArtists();
            List<Album> albums = sut.GetArtistAlbums(artists.First().id);
            List<Song> songs = sut.GetAlbumSongs(albums.First().id);

            //Assert
            Assert.IsNotNull(songs);
            Assert.IsTrue(songs.Count > 0);
        }

        [TestMethod]
        public void GetArtistTest()
        {
            //Arrange
            IAmpacheApi sut = CreateSut();

            //Act
            List<Artist> artists = sut.GetAllArtists();
            Artist artist = sut.GetArtist(artists.First().id);

            //Assert
            Assert.IsNotNull(artist);
            Assert.IsNotNull(artist.id);
            Assert.IsNotNull(artist.name);
        }

        [TestMethod]
        public void GetAlbumTest()
        {
            //Arrange
            IAmpacheApi sut = CreateSut();

            //Act
            List<Artist> artists = sut.GetAllArtists();
            List<Album> albums = sut.GetArtistAlbums(artists.First().id);
            Album album = sut.GetAlbum(albums.First().id);

            //Assert
            Assert.IsNotNull(album);
            Assert.IsNotNull(album.id);
            Assert.IsNotNull(album.name);
        }

        public AmpacheApi CreateSut(IAmpacheAuthInfo authInfo = null)
        {
            return new AmpacheApi(authInfo ?? _ampacheAuthInfo);
        }
    }
}
