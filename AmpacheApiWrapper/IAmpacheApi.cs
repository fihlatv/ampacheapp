﻿using AmpacheApiWrapper.DataTypes;
using System.Collections.Generic;

namespace AmpacheApiWrapper
{
    public interface IAmpacheApi
    {
        bool Login();
        List<Artist> GetAllArtists();
        Artist GetArtist(long artistId);
        List<Album> GetArtistAlbums(long artistId);
        Album GetAlbum(long albumId);
        List<Song> GetAlbumSongs(long filter);
    }
}
